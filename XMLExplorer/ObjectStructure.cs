﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text;
using System.Reflection;
namespace ObjectExplore
{
    public class ObjectStructure
    {
        public void main()
        {
            demoWalkTuple();
        }

        public void demoWalkTuple()
        {
            TupleHolder holdthing = new TupleHolder();
            propertyWalkToDebug(holdthing);
            ComplicatedLists complicatedThing = new ComplicatedLists();
            //propertyWalkToDebug(complicatedThing);
        }

        public string Serialize(object toSerialize)
        {
            //maybe serialize should be an object level
            var toSer = toSerialize;
            Type serTyp = toSer.GetType();
            XmlSerializer xsSubmit = new XmlSerializer(serTyp);
            String xml = "";
            XmlWriterSettings xmlSettings = new XmlWriterSettings();
            xmlSettings.Encoding = System.Text.Encoding.UTF8;
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww, xmlSettings))
            {
                xsSubmit.Serialize(writer, toSer);
                xml = sww.ToString(); // Your XML
            }
            //return string.Format("{0}", xml);
            return xml;
        }

        public MemoryStream SerializeToStream(object toSerialize)
        {
            //maybe serialize should be an object level
            var toSer = toSerialize;
            Type serTyp = toSer.GetType();
            XmlSerializer xsSubmit = new XmlSerializer(serTyp);
            String xml = "";
            XmlWriterSettings xmlSettings = new XmlWriterSettings();
            xmlSettings.Encoding = System.Text.Encoding.UTF8;
            using (StringWriter sww = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sww, xmlSettings))
            {
                xsSubmit.Serialize(writer, toSer);
                xml = sww.ToString(); // Your XML
            }
            MemoryStream ms = new MemoryStream();
            byte[] xmlBA = Encoding.UTF8.GetBytes(xml);
            return ms;
        }

        /// <summary>
        /// Based on StackOverflow solution
        /// http://stackoverflow.com/questions/6196413/how-to-recursively-print-the-values-of-an-objects-properties-using-reflection
        ///
        /// To create a map between an XML schema and available data, we must be able to address each type.
        /// Using reflection, this generates a nested list of properties and their types.
        /// This can then be used to generate a graphical interface, such as an HTML form,
        /// to assign 1:1 associations between metadata and XML elements.
        ///
        /// This facilitates a method of automating XML output based on XSD which allows the system to
        /// keep the schema current automatically by
        /// 1. Checking for an update to the schema.
        /// 2. Collecting all the required XSD files.
        /// 3. Converting that schema to a class.
        /// 4. Exposing the class structure to an interface a non-technical user can understand.
        /// 5. Letting the non-technical user map available data to the class's properties
        /// 6. Saving that map for use again later, in such a way that an updated schema will non-destructively allow for an update to the map.
        /// 7. Then be called together with live product information to produce XML data which fits the spec loaded at the beginning of the process (via AutoMapper between XSD class and metadata adapter class).
        /// </summary>
        /// <param name="toWalk"></param>
        /// //TODO: move this to it's own class
        public void propertyWalkToDebug(object toWalk)
        {
            propertyWalk(toWalk.GetType(), new List<PropertyInfo>(), PrintPropertyAncestry);
        }

        /// <summary>
        /// Serialize the properties of an object, including its subtypes down to System namespace properties, including the ancestry between the root type and the present type, without risk of infinite recursion.
        /// </summary>
        /// <param name="toWalk">type whose properties we want to serialize through reflection</param>
        /// <param name="antecedants">list of strings of a property's direct ancestry</param>
        /// <param name="outMethod">void method to operate on the serializaton of a property and it's ancestry</param>
        /// <param name="listToString">string method serializing a property's ancestry</param>
        private void propertyWalk(Type toWalk, List<string> antecedants, Action<string> outMethod, Func<IEnumerable<string>, string, string> listToString)
        {
            foreach (PropertyInfo pInfo in toWalk.GetTypeInfo().DeclaredProperties)
            {
                outMethod(listToString(antecedants, ":") + pInfo.Name + "(" + pInfo.PropertyType + ")");
                if (antecedants.Contains(pInfo.PropertyType.Name)) continue; //this prevents types which are in their own ancestry from being reiterated
                if (pInfo.PropertyType.Namespace != null)
                {
                    if (pInfo.PropertyType.Namespace.Equals("System")) continue; //properties of types in the System namespace are not useful in this application.
                }
                List<string> nextAntecedants = new List<string>(antecedants);
                nextAntecedants.Add(pInfo.PropertyType.Name);
                //TODO: what will this do in a list of tuples???
                if (pInfo.PropertyType.GetTypeInfo().DeclaredProperties.Count() > 1)
                    propertyWalk(
                        pInfo.PropertyType.GetTypeInfo().ImplementedInterfaces.Contains(typeof(IEnumerable)) ? //expand on type Collection will list, not type of the Collection itself.
                            pInfo.PropertyType.GetElementType()
                            : pInfo.PropertyType
                        , nextAntecedants
                        , outMethod
                        , listToString
                        );
            }
        }

        private void propertyWalk(Type toWalk, List<PropertyInfo> antecedants, Action<List<PropertyInfo>, PropertyInfo> outMethod)
        {
            foreach (PropertyInfo pInfo in toWalk.GetTypeInfo().DeclaredProperties)
            {
                outMethod(antecedants, pInfo);
                if (antecedants.Exists(x => x.PropertyType == pInfo.PropertyType)) continue; //this prevents types which are in their own ancestry from being reiterated
                if (pInfo.PropertyType.Namespace != null)
                {
                    //this ALSO blocks out tuples, which is not desirable...
                    if (pInfo.PropertyType.Namespace.Equals("System")) continue; //properties of types in the System namespace are not useful in this application.
                }
                List<PropertyInfo> nextAntecedants = new List<PropertyInfo>(antecedants);
                nextAntecedants.Add(pInfo);
                if (pInfo.PropertyType.GetTypeInfo().DeclaredProperties.Count() > 1)
                    propertyWalk(
                        pInfo.PropertyType.GetTypeInfo().ImplementedInterfaces.Contains(typeof(IEnumerable)) ? //expand on type Collection will list, not type of the Collection itself.
                            pInfo.PropertyType.GetElementType()
                            : pInfo.PropertyType
                        , nextAntecedants
                        , outMethod
                        );
                //TODO: handle 1+n dimensional lists? current output for propertyWalkToDebug(typeof(TupleHolder)) == stringsList(System.Tuple`2[System.String,System.String])
                //That's no good if the types in the Tuple aren't System types.
            }
        }

        public void PrintPropertyAncestry(List<PropertyInfo> antecedants, PropertyInfo pInfo)
        {
            toDebugConsole(ListToString(antecedants, ":") + pInfo.Name + "(" + pInfo.PropertyType + ")");
        }

        public void toDebugConsole(string input)
        {
            System.Diagnostics.Debug.WriteLine(input);
        }

        public string ListToString(IEnumerable<string> listIn, string seperator)
        {
            string output = "";
            foreach (string item in listIn)
            {
                output += item + seperator;
            }
            return output;
        }

        public string ListToString(IEnumerable<PropertyInfo> listIn, string seperator)
        {
            string output = "";
            foreach (PropertyInfo item in listIn)
            {
                output += item.Name + seperator;
            }
            return output;
        }

        public class TupleHolder
        {
            private Tuple<string, string> _stringsList;
            //private Tuple<PropertyInfo, Type> _propTypeList;
            public Tuple<string, string> stringsList { get { return _stringsList; } }
            //public Tuple<PropertyInfo, Type> propTypeList { get { return _propTypeList; } }
        }

        public class ComplicatedLists
        {
            private Dictionary<string, TupleHolder> _namedTupleHolders;
            public Dictionary<string, TupleHolder> namedTupleHolders { get { return _namedTupleHolders; } }
        }
    }
}